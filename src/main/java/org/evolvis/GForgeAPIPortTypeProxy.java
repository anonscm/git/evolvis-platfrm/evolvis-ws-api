/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis;

public class GForgeAPIPortTypeProxy implements org.evolvis.GForgeAPIPortType {
  private String _endpoint = null;
  private org.evolvis.GForgeAPIPortType gForgeAPIPortType = null;
  
  public GForgeAPIPortTypeProxy() {
    _initGForgeAPIPortTypeProxy();
  }
  
  private void _initGForgeAPIPortTypeProxy() {
    try {
      gForgeAPIPortType = (new org.evolvis.GForgeAPILocator()).getGForgeAPIPort();
      if (gForgeAPIPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)gForgeAPIPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)gForgeAPIPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (gForgeAPIPortType != null)
      ((javax.xml.rpc.Stub)gForgeAPIPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.evolvis.GForgeAPIPortType getGForgeAPIPortType() {
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType;
  }
  
  public java.lang.String login(java.lang.String userid, java.lang.String passwd) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.login(userid, passwd);
  }
  
  public java.lang.String logout(java.lang.String session_ser) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.logout(session_ser);
  }
  
  public org.evolvis.Group[] getGroups(java.lang.String session_ser, int[] group_ids) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getGroups(session_ser, group_ids);
  }
  
  public org.evolvis.Group[] getGroupsByName(java.lang.String session_ser, java.lang.String[] group_names) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getGroupsByName(session_ser, group_names);
  }
  
  public java.lang.String[] getPublicProjectNames(java.lang.String session_ser) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getPublicProjectNames(session_ser);
  }
  
  public org.evolvis.User[] getUsers(java.lang.String session_ser, int[] user_ids) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getUsers(session_ser, user_ids);
  }
  
  public org.evolvis.User[] getUsersByName(java.lang.String session_ser, java.lang.String[] user_ids) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getUsersByName(session_ser, user_ids);
  }
  
  public org.evolvis.Group[] userGetGroups(java.lang.String session_ser, int user_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.userGetGroups(session_ser, user_id);
  }
  
  public org.evolvis.ArtifactType[] getArtifactTypes(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifactTypes(session_ser, group_id);
  }
  
  public org.evolvis.Artifact[] getArtifacts(java.lang.String session_ser, int group_id, int group_artifact_id, int assigned_to, int status) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifacts(session_ser, group_id, group_artifact_id, assigned_to, status);
  }
  
  public int addArtifact(java.lang.String session_ser, int group_id, int group_artifact_id, int status_id, int priority, int assigned_to, java.lang.String summary, java.lang.String details, org.evolvis.ArtifactExtraFieldsData[] extra_fields) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addArtifact(session_ser, group_id, group_artifact_id, status_id, priority, assigned_to, summary, details, extra_fields);
  }
  
  public int updateArtifact(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int status_id, int priority, int assigned_to, java.lang.String summary, java.lang.String details, int new_artifact_type_id, org.evolvis.ArtifactExtraFieldsData[] extra_fields_data) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.updateArtifact(session_ser, group_id, group_artifact_id, artifact_id, status_id, priority, assigned_to, summary, details, new_artifact_type_id, extra_fields_data);
  }
  
  public org.evolvis.ArtifactFile[] getArtifactFiles(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifactFiles(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public java.lang.String getArtifactFileData(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int file_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifactFileData(session_ser, group_id, group_artifact_id, artifact_id, file_id);
  }
  
  public int addArtifactFile(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, java.lang.String base64_contents, java.lang.String description, java.lang.String filename, java.lang.String filetype) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addArtifactFile(session_ser, group_id, group_artifact_id, artifact_id, base64_contents, description, filename, filetype);
  }
  
  public boolean artifactFileDelete(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int file_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactFileDelete(session_ser, group_id, group_artifact_id, artifact_id, file_id);
  }
  
  public org.evolvis.ArtifactMessage[] getArtifactMessages(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifactMessages(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public int addArtifactMessage(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, java.lang.String body) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addArtifactMessage(session_ser, group_id, group_artifact_id, artifact_id, body);
  }
  
  public org.evolvis.User[] getArtifactTechnicians(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getArtifactTechnicians(session_ser, group_id, group_artifact_id);
  }
  
  public boolean artifactSetMonitor(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactSetMonitor(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public boolean artifactIsMonitoring(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactIsMonitoring(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public boolean artifactDelete(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactDelete(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public boolean artifactTypeIsMonitoring(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactTypeIsMonitoring(session_ser, group_id, group_artifact_id);
  }
  
  public org.evolvis.ArtifactChangeLog[] artifactGetChangeLog(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactGetChangeLog(session_ser, group_id, group_artifact_id, artifact_id);
  }
  
  public org.evolvis.ArtifactQuery[] artifactGetViews(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactGetViews(session_ser, group_id, group_artifact_id);
  }
  
  public boolean artifactDeleteView(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_query_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactDeleteView(session_ser, group_id, group_artifact_id, artifact_query_id);
  }
  
  public boolean artifactSetView(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_query_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactSetView(session_ser, group_id, group_artifact_id, artifact_query_id);
  }
  
  public int artifactCreateView(java.lang.String session_ser, int group_id, int group_artifact_id, java.lang.String name, int status, int[] assignee, java.lang.String moddaterange, java.lang.String sort_col, java.lang.String sort_ord, org.evolvis.ArtifactExtraFieldsData[] extra_fields, java.lang.String opendaterange, java.lang.String closedaterange) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactCreateView(session_ser, group_id, group_artifact_id, name, status, assignee, moddaterange, sort_col, sort_ord, extra_fields, opendaterange, closedaterange);
  }
  
  public int artifactUpdateView(java.lang.String session_ser, int group_id, int group_artifact_id, int query_id, java.lang.String name, int status, int[] assignee, java.lang.String moddaterange, java.lang.String sort_col, java.lang.String sort_ord, org.evolvis.ArtifactExtraFieldsData[] extra_fields, java.lang.String opendaterange, java.lang.String closedaterange) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.artifactUpdateView(session_ser, group_id, group_artifact_id, query_id, name, status, assignee, moddaterange, sort_col, sort_ord, extra_fields, opendaterange, closedaterange);
  }
  
  public org.evolvis.ProjectGroup[] getProjectGroups(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getProjectGroups(session_ser, group_id);
  }
  
  public org.evolvis.ProjectTask[] getProjectTasks(java.lang.String session_ser, int group_id, int group_project_id, int assigned_to, int status, int category, int group) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getProjectTasks(session_ser, group_id, group_project_id, assigned_to, status, category, group);
  }
  
  public int addProjectTask(java.lang.String session_ser, int group_id, int group_project_id, java.lang.String summary, java.lang.String details, int priority, int hours, int start_date, int end_date, int category_id, int percent_complete, int[] assigned_to, int[] dependent_on, int duration, int parent_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addProjectTask(session_ser, group_id, group_project_id, summary, details, priority, hours, start_date, end_date, category_id, percent_complete, assigned_to, dependent_on, duration, parent_id);
  }
  
  public int updateProjectTask(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id, java.lang.String summary, java.lang.String details, int priority, int hours, int start_date, int end_date, int status_id, int category_id, int percent_complete, int[] assigned_to, int[] dependent_on, int new_group_project_id, int duration, int parent_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.updateProjectTask(session_ser, group_id, group_project_id, project_task_id, summary, details, priority, hours, start_date, end_date, status_id, category_id, percent_complete, assigned_to, dependent_on, new_group_project_id, duration, parent_id);
  }
  
  public org.evolvis.ProjectCategory[] getProjectTaskCategories(java.lang.String session_ser, int group_id, int group_project_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getProjectTaskCategories(session_ser, group_id, group_project_id);
  }
  
  public org.evolvis.ProjectMessage[] getProjectMessages(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getProjectMessages(session_ser, group_id, group_project_id, project_task_id);
  }
  
  public int addProjectMessage(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id, java.lang.String body) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addProjectMessage(session_ser, group_id, group_project_id, project_task_id, body);
  }
  
  public org.evolvis.User[] getProjectTechnicians(java.lang.String session_ser, int group_id, int group_project_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getProjectTechnicians(session_ser, group_id, group_project_id);
  }
  
  public org.evolvis.FRSPackage[] getPackages(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getPackages(session_ser, group_id);
  }
  
  public int addPackage(java.lang.String session_ser, int group_id, java.lang.String package_name, int is_public) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addPackage(session_ser, group_id, package_name, is_public);
  }
  
  public org.evolvis.FRSRelease[] getReleases(java.lang.String session_ser, int group_id, int package_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getReleases(session_ser, group_id, package_id);
  }
  
  public int addRelease(java.lang.String session_ser, int group_id, int package_id, java.lang.String name, java.lang.String notes, java.lang.String changes, int release_date) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addRelease(session_ser, group_id, package_id, name, notes, changes, release_date);
  }
  
  public org.evolvis.FRSFile[] getFiles(java.lang.String session_ser, int group_id, int package_id, int release_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getFiles(session_ser, group_id, package_id, release_id);
  }
  
  public java.lang.String getFile(java.lang.String session_ser, int group_id, int package_id, int release_id, int file_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getFile(session_ser, group_id, package_id, release_id, file_id);
  }
  
  public java.lang.String addFile(java.lang.String session_ser, int group_id, int package_id, int release_id, java.lang.String name, java.lang.String base64_contents, int type_id, int processor_id, int release_time) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addFile(session_ser, group_id, package_id, release_id, name, base64_contents, type_id, processor_id, release_time);
  }
  
  public org.evolvis.DocumentState[] getDocumentStates(java.lang.String session_ser) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocumentStates(session_ser);
  }
  
  public org.evolvis.DocumentLanguage[] getDocumentLanguages(java.lang.String session_ser) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocumentLanguages(session_ser);
  }
  
  public int addDocument(java.lang.String session_ser, int group_id, int doc_group, java.lang.String title, java.lang.String description, int language_id, java.lang.String base64_contents, java.lang.String filename, java.lang.String file_url) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addDocument(session_ser, group_id, doc_group, title, description, language_id, base64_contents, filename, file_url);
  }
  
  public boolean updateDocument(java.lang.String session_ser, int group_id, int doc_group, int doc_id, java.lang.String title, java.lang.String description, int language_id, java.lang.String base64_contents, java.lang.String filename, java.lang.String file_url, int state_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.updateDocument(session_ser, group_id, doc_group, doc_id, title, description, language_id, base64_contents, filename, file_url, state_id);
  }
  
  public int addDocumentGroup(java.lang.String session_ser, int group_id, java.lang.String groupname, int parent_doc_group) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.addDocumentGroup(session_ser, group_id, groupname, parent_doc_group);
  }
  
  public boolean updateDocumentGroup(java.lang.String session_ser, int group_id, int doc_group, java.lang.String new_groupname, int new_parent_doc_group) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.updateDocumentGroup(session_ser, group_id, doc_group, new_groupname, new_parent_doc_group);
  }
  
  public org.evolvis.Document[] getDocuments(java.lang.String session_ser, int group_id, int doc_group) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocuments(session_ser, group_id, doc_group);
  }
  
  public org.evolvis.DocumentGroup[] getDocumentGroups(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocumentGroups(session_ser, group_id);
  }
  
  public org.evolvis.DocumentGroup getDocumentGroup(java.lang.String session_ser, int group_id, int doc_group) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocumentGroup(session_ser, group_id, doc_group);
  }
  
  public org.evolvis.DocumentFile[] getDocumentFiles(java.lang.String session_ser, int group_id, int doc_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.getDocumentFiles(session_ser, group_id, doc_id);
  }
  
  public boolean documentDelete(java.lang.String session_ser, int group_id, int doc_id) throws java.rmi.RemoteException{
    if (gForgeAPIPortType == null)
      _initGForgeAPIPortTypeProxy();
    return gForgeAPIPortType.documentDelete(session_ser, group_id, doc_id);
  }

}