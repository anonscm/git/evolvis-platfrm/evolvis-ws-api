/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ArtifactType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ArtifactType  implements java.io.Serializable {
    private int group_artifact_id;

    private int group_id;

    private java.lang.String name;

    private java.lang.String description;

    private int is_public;

    private int allow_anon;

    private int due_period;

    private int datatype;

    private int status_timeout;

    private org.evolvis.ArtifactExtraField[] extra_fields;

    private int custom_status_field;

    public ArtifactType() {
    }

    public ArtifactType(
           int group_artifact_id,
           int group_id,
           java.lang.String name,
           java.lang.String description,
           int is_public,
           int allow_anon,
           int due_period,
           int datatype,
           int status_timeout,
           org.evolvis.ArtifactExtraField[] extra_fields,
           int custom_status_field) {
           this.group_artifact_id = group_artifact_id;
           this.group_id = group_id;
           this.name = name;
           this.description = description;
           this.is_public = is_public;
           this.allow_anon = allow_anon;
           this.due_period = due_period;
           this.datatype = datatype;
           this.status_timeout = status_timeout;
           this.extra_fields = extra_fields;
           this.custom_status_field = custom_status_field;
    }


    /**
     * Gets the group_artifact_id value for this ArtifactType.
     * 
     * @return group_artifact_id
     */
    public int getGroup_artifact_id() {
        return group_artifact_id;
    }


    /**
     * Sets the group_artifact_id value for this ArtifactType.
     * 
     * @param group_artifact_id
     */
    public void setGroup_artifact_id(int group_artifact_id) {
        this.group_artifact_id = group_artifact_id;
    }


    /**
     * Gets the group_id value for this ArtifactType.
     * 
     * @return group_id
     */
    public int getGroup_id() {
        return group_id;
    }


    /**
     * Sets the group_id value for this ArtifactType.
     * 
     * @param group_id
     */
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }


    /**
     * Gets the name value for this ArtifactType.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ArtifactType.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the description value for this ArtifactType.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this ArtifactType.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the is_public value for this ArtifactType.
     * 
     * @return is_public
     */
    public int getIs_public() {
        return is_public;
    }


    /**
     * Sets the is_public value for this ArtifactType.
     * 
     * @param is_public
     */
    public void setIs_public(int is_public) {
        this.is_public = is_public;
    }


    /**
     * Gets the allow_anon value for this ArtifactType.
     * 
     * @return allow_anon
     */
    public int getAllow_anon() {
        return allow_anon;
    }


    /**
     * Sets the allow_anon value for this ArtifactType.
     * 
     * @param allow_anon
     */
    public void setAllow_anon(int allow_anon) {
        this.allow_anon = allow_anon;
    }


    /**
     * Gets the due_period value for this ArtifactType.
     * 
     * @return due_period
     */
    public int getDue_period() {
        return due_period;
    }


    /**
     * Sets the due_period value for this ArtifactType.
     * 
     * @param due_period
     */
    public void setDue_period(int due_period) {
        this.due_period = due_period;
    }


    /**
     * Gets the datatype value for this ArtifactType.
     * 
     * @return datatype
     */
    public int getDatatype() {
        return datatype;
    }


    /**
     * Sets the datatype value for this ArtifactType.
     * 
     * @param datatype
     */
    public void setDatatype(int datatype) {
        this.datatype = datatype;
    }


    /**
     * Gets the status_timeout value for this ArtifactType.
     * 
     * @return status_timeout
     */
    public int getStatus_timeout() {
        return status_timeout;
    }


    /**
     * Sets the status_timeout value for this ArtifactType.
     * 
     * @param status_timeout
     */
    public void setStatus_timeout(int status_timeout) {
        this.status_timeout = status_timeout;
    }


    /**
     * Gets the extra_fields value for this ArtifactType.
     * 
     * @return extra_fields
     */
    public org.evolvis.ArtifactExtraField[] getExtra_fields() {
        return extra_fields;
    }


    /**
     * Sets the extra_fields value for this ArtifactType.
     * 
     * @param extra_fields
     */
    public void setExtra_fields(org.evolvis.ArtifactExtraField[] extra_fields) {
        this.extra_fields = extra_fields;
    }


    /**
     * Gets the custom_status_field value for this ArtifactType.
     * 
     * @return custom_status_field
     */
    public int getCustom_status_field() {
        return custom_status_field;
    }


    /**
     * Sets the custom_status_field value for this ArtifactType.
     * 
     * @param custom_status_field
     */
    public void setCustom_status_field(int custom_status_field) {
        this.custom_status_field = custom_status_field;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArtifactType)) return false;
        ArtifactType other = (ArtifactType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.group_artifact_id == other.getGroup_artifact_id() &&
            this.group_id == other.getGroup_id() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            this.is_public == other.getIs_public() &&
            this.allow_anon == other.getAllow_anon() &&
            this.due_period == other.getDue_period() &&
            this.datatype == other.getDatatype() &&
            this.status_timeout == other.getStatus_timeout() &&
            ((this.extra_fields==null && other.getExtra_fields()==null) || 
             (this.extra_fields!=null &&
              java.util.Arrays.equals(this.extra_fields, other.getExtra_fields()))) &&
            this.custom_status_field == other.getCustom_status_field();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGroup_artifact_id();
        _hashCode += getGroup_id();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        _hashCode += getIs_public();
        _hashCode += getAllow_anon();
        _hashCode += getDue_period();
        _hashCode += getDatatype();
        _hashCode += getStatus_timeout();
        if (getExtra_fields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExtra_fields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtra_fields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getCustom_status_field();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArtifactType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_artifact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_artifact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_public");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_public"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allow_anon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allow_anon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("due_period");
        elemField.setXmlName(new javax.xml.namespace.QName("", "due_period"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datatype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datatype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_timeout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_timeout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_fields");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_fields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraField"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_status_field");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_status_field"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
