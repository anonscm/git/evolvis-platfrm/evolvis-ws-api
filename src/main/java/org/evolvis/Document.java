/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * Document.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class Document  implements java.io.Serializable {
    private int docid;

    private int doc_group;

    private java.lang.String title;

    private java.lang.String description;

    private int stateid;

    private int language_id;

    private int filesize;

    public Document() {
    }

    public Document(
           int docid,
           int doc_group,
           java.lang.String title,
           java.lang.String description,
           int stateid,
           int language_id,
           int filesize) {
           this.docid = docid;
           this.doc_group = doc_group;
           this.title = title;
           this.description = description;
           this.stateid = stateid;
           this.language_id = language_id;
           this.filesize = filesize;
    }


    /**
     * Gets the docid value for this Document.
     * 
     * @return docid
     */
    public int getDocid() {
        return docid;
    }


    /**
     * Sets the docid value for this Document.
     * 
     * @param docid
     */
    public void setDocid(int docid) {
        this.docid = docid;
    }


    /**
     * Gets the doc_group value for this Document.
     * 
     * @return doc_group
     */
    public int getDoc_group() {
        return doc_group;
    }


    /**
     * Sets the doc_group value for this Document.
     * 
     * @param doc_group
     */
    public void setDoc_group(int doc_group) {
        this.doc_group = doc_group;
    }


    /**
     * Gets the title value for this Document.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Document.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the description value for this Document.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Document.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the stateid value for this Document.
     * 
     * @return stateid
     */
    public int getStateid() {
        return stateid;
    }


    /**
     * Sets the stateid value for this Document.
     * 
     * @param stateid
     */
    public void setStateid(int stateid) {
        this.stateid = stateid;
    }


    /**
     * Gets the language_id value for this Document.
     * 
     * @return language_id
     */
    public int getLanguage_id() {
        return language_id;
    }


    /**
     * Sets the language_id value for this Document.
     * 
     * @param language_id
     */
    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }


    /**
     * Gets the filesize value for this Document.
     * 
     * @return filesize
     */
    public int getFilesize() {
        return filesize;
    }


    /**
     * Sets the filesize value for this Document.
     * 
     * @param filesize
     */
    public void setFilesize(int filesize) {
        this.filesize = filesize;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Document)) return false;
        Document other = (Document) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.docid == other.getDocid() &&
            this.doc_group == other.getDoc_group() &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            this.stateid == other.getStateid() &&
            this.language_id == other.getLanguage_id() &&
            this.filesize == other.getFilesize();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDocid();
        _hashCode += getDoc_group();
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        _hashCode += getStateid();
        _hashCode += getLanguage_id();
        _hashCode += getFilesize();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Document.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "Document"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "docid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("doc_group");
        elemField.setXmlName(new javax.xml.namespace.QName("", "doc_group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "stateid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("language_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "language_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filesize");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filesize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
