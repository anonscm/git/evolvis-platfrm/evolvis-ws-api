/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ProjectMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ProjectMessage  implements java.io.Serializable {
    private int project_message_id;

    private int project_task_id;

    private java.lang.String body;

    private int postdate;

    private int posted_by;

    public ProjectMessage() {
    }

    public ProjectMessage(
           int project_message_id,
           int project_task_id,
           java.lang.String body,
           int postdate,
           int posted_by) {
           this.project_message_id = project_message_id;
           this.project_task_id = project_task_id;
           this.body = body;
           this.postdate = postdate;
           this.posted_by = posted_by;
    }


    /**
     * Gets the project_message_id value for this ProjectMessage.
     * 
     * @return project_message_id
     */
    public int getProject_message_id() {
        return project_message_id;
    }


    /**
     * Sets the project_message_id value for this ProjectMessage.
     * 
     * @param project_message_id
     */
    public void setProject_message_id(int project_message_id) {
        this.project_message_id = project_message_id;
    }


    /**
     * Gets the project_task_id value for this ProjectMessage.
     * 
     * @return project_task_id
     */
    public int getProject_task_id() {
        return project_task_id;
    }


    /**
     * Sets the project_task_id value for this ProjectMessage.
     * 
     * @param project_task_id
     */
    public void setProject_task_id(int project_task_id) {
        this.project_task_id = project_task_id;
    }


    /**
     * Gets the body value for this ProjectMessage.
     * 
     * @return body
     */
    public java.lang.String getBody() {
        return body;
    }


    /**
     * Sets the body value for this ProjectMessage.
     * 
     * @param body
     */
    public void setBody(java.lang.String body) {
        this.body = body;
    }


    /**
     * Gets the postdate value for this ProjectMessage.
     * 
     * @return postdate
     */
    public int getPostdate() {
        return postdate;
    }


    /**
     * Sets the postdate value for this ProjectMessage.
     * 
     * @param postdate
     */
    public void setPostdate(int postdate) {
        this.postdate = postdate;
    }


    /**
     * Gets the posted_by value for this ProjectMessage.
     * 
     * @return posted_by
     */
    public int getPosted_by() {
        return posted_by;
    }


    /**
     * Sets the posted_by value for this ProjectMessage.
     * 
     * @param posted_by
     */
    public void setPosted_by(int posted_by) {
        this.posted_by = posted_by;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProjectMessage)) return false;
        ProjectMessage other = (ProjectMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.project_message_id == other.getProject_message_id() &&
            this.project_task_id == other.getProject_task_id() &&
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              this.body.equals(other.getBody()))) &&
            this.postdate == other.getPostdate() &&
            this.posted_by == other.getPosted_by();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getProject_message_id();
        _hashCode += getProject_task_id();
        if (getBody() != null) {
            _hashCode += getBody().hashCode();
        }
        _hashCode += getPostdate();
        _hashCode += getPosted_by();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProjectMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ProjectMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("project_message_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "project_message_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("project_task_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "project_task_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("", "body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "postdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posted_by");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posted_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
