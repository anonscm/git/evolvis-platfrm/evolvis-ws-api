/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * DocumentGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class DocumentGroup  implements java.io.Serializable {
    private int doc_group_id;

    private int parent_doc_group;

    private java.lang.String groupname;

    public DocumentGroup() {
    }

    public DocumentGroup(
           int doc_group_id,
           int parent_doc_group,
           java.lang.String groupname) {
           this.doc_group_id = doc_group_id;
           this.parent_doc_group = parent_doc_group;
           this.groupname = groupname;
    }


    /**
     * Gets the doc_group_id value for this DocumentGroup.
     * 
     * @return doc_group_id
     */
    public int getDoc_group_id() {
        return doc_group_id;
    }


    /**
     * Sets the doc_group_id value for this DocumentGroup.
     * 
     * @param doc_group_id
     */
    public void setDoc_group_id(int doc_group_id) {
        this.doc_group_id = doc_group_id;
    }


    /**
     * Gets the parent_doc_group value for this DocumentGroup.
     * 
     * @return parent_doc_group
     */
    public int getParent_doc_group() {
        return parent_doc_group;
    }


    /**
     * Sets the parent_doc_group value for this DocumentGroup.
     * 
     * @param parent_doc_group
     */
    public void setParent_doc_group(int parent_doc_group) {
        this.parent_doc_group = parent_doc_group;
    }


    /**
     * Gets the groupname value for this DocumentGroup.
     * 
     * @return groupname
     */
    public java.lang.String getGroupname() {
        return groupname;
    }


    /**
     * Sets the groupname value for this DocumentGroup.
     * 
     * @param groupname
     */
    public void setGroupname(java.lang.String groupname) {
        this.groupname = groupname;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentGroup)) return false;
        DocumentGroup other = (DocumentGroup) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.doc_group_id == other.getDoc_group_id() &&
            this.parent_doc_group == other.getParent_doc_group() &&
            ((this.groupname==null && other.getGroupname()==null) || 
             (this.groupname!=null &&
              this.groupname.equals(other.getGroupname())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDoc_group_id();
        _hashCode += getParent_doc_group();
        if (getGroupname() != null) {
            _hashCode += getGroupname().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentGroup.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "DocumentGroup"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("doc_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "doc_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parent_doc_group");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parent_doc_group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupname");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
