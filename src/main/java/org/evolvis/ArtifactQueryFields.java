/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ArtifactQueryFields.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ArtifactQueryFields  implements java.io.Serializable {
    private java.lang.String sortcol;

    private java.lang.String sortord;

    private java.lang.String moddaterange;

    private java.math.BigInteger[] assignee;

    private int status;

    private org.evolvis.ArtifactQueryExtraField[] extra_fields;

    private java.lang.String opendaterange;

    private java.lang.String closedaterange;

    public ArtifactQueryFields() {
    }

    public ArtifactQueryFields(
           java.lang.String sortcol,
           java.lang.String sortord,
           java.lang.String moddaterange,
           java.math.BigInteger[] assignee,
           int status,
           org.evolvis.ArtifactQueryExtraField[] extra_fields,
           java.lang.String opendaterange,
           java.lang.String closedaterange) {
           this.sortcol = sortcol;
           this.sortord = sortord;
           this.moddaterange = moddaterange;
           this.assignee = assignee;
           this.status = status;
           this.extra_fields = extra_fields;
           this.opendaterange = opendaterange;
           this.closedaterange = closedaterange;
    }


    /**
     * Gets the sortcol value for this ArtifactQueryFields.
     * 
     * @return sortcol
     */
    public java.lang.String getSortcol() {
        return sortcol;
    }


    /**
     * Sets the sortcol value for this ArtifactQueryFields.
     * 
     * @param sortcol
     */
    public void setSortcol(java.lang.String sortcol) {
        this.sortcol = sortcol;
    }


    /**
     * Gets the sortord value for this ArtifactQueryFields.
     * 
     * @return sortord
     */
    public java.lang.String getSortord() {
        return sortord;
    }


    /**
     * Sets the sortord value for this ArtifactQueryFields.
     * 
     * @param sortord
     */
    public void setSortord(java.lang.String sortord) {
        this.sortord = sortord;
    }


    /**
     * Gets the moddaterange value for this ArtifactQueryFields.
     * 
     * @return moddaterange
     */
    public java.lang.String getModdaterange() {
        return moddaterange;
    }


    /**
     * Sets the moddaterange value for this ArtifactQueryFields.
     * 
     * @param moddaterange
     */
    public void setModdaterange(java.lang.String moddaterange) {
        this.moddaterange = moddaterange;
    }


    /**
     * Gets the assignee value for this ArtifactQueryFields.
     * 
     * @return assignee
     */
    public java.math.BigInteger[] getAssignee() {
        return assignee;
    }


    /**
     * Sets the assignee value for this ArtifactQueryFields.
     * 
     * @param assignee
     */
    public void setAssignee(java.math.BigInteger[] assignee) {
        this.assignee = assignee;
    }


    /**
     * Gets the status value for this ArtifactQueryFields.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ArtifactQueryFields.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the extra_fields value for this ArtifactQueryFields.
     * 
     * @return extra_fields
     */
    public org.evolvis.ArtifactQueryExtraField[] getExtra_fields() {
        return extra_fields;
    }


    /**
     * Sets the extra_fields value for this ArtifactQueryFields.
     * 
     * @param extra_fields
     */
    public void setExtra_fields(org.evolvis.ArtifactQueryExtraField[] extra_fields) {
        this.extra_fields = extra_fields;
    }


    /**
     * Gets the opendaterange value for this ArtifactQueryFields.
     * 
     * @return opendaterange
     */
    public java.lang.String getOpendaterange() {
        return opendaterange;
    }


    /**
     * Sets the opendaterange value for this ArtifactQueryFields.
     * 
     * @param opendaterange
     */
    public void setOpendaterange(java.lang.String opendaterange) {
        this.opendaterange = opendaterange;
    }


    /**
     * Gets the closedaterange value for this ArtifactQueryFields.
     * 
     * @return closedaterange
     */
    public java.lang.String getClosedaterange() {
        return closedaterange;
    }


    /**
     * Sets the closedaterange value for this ArtifactQueryFields.
     * 
     * @param closedaterange
     */
    public void setClosedaterange(java.lang.String closedaterange) {
        this.closedaterange = closedaterange;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArtifactQueryFields)) return false;
        ArtifactQueryFields other = (ArtifactQueryFields) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sortcol==null && other.getSortcol()==null) || 
             (this.sortcol!=null &&
              this.sortcol.equals(other.getSortcol()))) &&
            ((this.sortord==null && other.getSortord()==null) || 
             (this.sortord!=null &&
              this.sortord.equals(other.getSortord()))) &&
            ((this.moddaterange==null && other.getModdaterange()==null) || 
             (this.moddaterange!=null &&
              this.moddaterange.equals(other.getModdaterange()))) &&
            ((this.assignee==null && other.getAssignee()==null) || 
             (this.assignee!=null &&
              java.util.Arrays.equals(this.assignee, other.getAssignee()))) &&
            this.status == other.getStatus() &&
            ((this.extra_fields==null && other.getExtra_fields()==null) || 
             (this.extra_fields!=null &&
              java.util.Arrays.equals(this.extra_fields, other.getExtra_fields()))) &&
            ((this.opendaterange==null && other.getOpendaterange()==null) || 
             (this.opendaterange!=null &&
              this.opendaterange.equals(other.getOpendaterange()))) &&
            ((this.closedaterange==null && other.getClosedaterange()==null) || 
             (this.closedaterange!=null &&
              this.closedaterange.equals(other.getClosedaterange())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSortcol() != null) {
            _hashCode += getSortcol().hashCode();
        }
        if (getSortord() != null) {
            _hashCode += getSortord().hashCode();
        }
        if (getModdaterange() != null) {
            _hashCode += getModdaterange().hashCode();
        }
        if (getAssignee() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssignee());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssignee(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getStatus();
        if (getExtra_fields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExtra_fields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtra_fields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOpendaterange() != null) {
            _hashCode += getOpendaterange().hashCode();
        }
        if (getClosedaterange() != null) {
            _hashCode += getClosedaterange().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArtifactQueryFields.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactQueryFields"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortcol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sortcol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortord");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sortord"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moddaterange");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moddaterange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignee");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assignee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_fields");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_fields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactQueryExtraField"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opendaterange");
        elemField.setXmlName(new javax.xml.namespace.QName("", "opendaterange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closedaterange");
        elemField.setXmlName(new javax.xml.namespace.QName("", "closedaterange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
