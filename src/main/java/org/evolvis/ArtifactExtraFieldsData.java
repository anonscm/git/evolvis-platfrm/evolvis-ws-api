/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ArtifactExtraFieldsData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ArtifactExtraFieldsData  implements java.io.Serializable {
    private int extra_field_id;

    private java.lang.String field_data;

    public ArtifactExtraFieldsData() {
    }

    public ArtifactExtraFieldsData(
           int extra_field_id,
           java.lang.String field_data) {
           this.extra_field_id = extra_field_id;
           this.field_data = field_data;
    }


    /**
     * Gets the extra_field_id value for this ArtifactExtraFieldsData.
     * 
     * @return extra_field_id
     */
    public int getExtra_field_id() {
        return extra_field_id;
    }


    /**
     * Sets the extra_field_id value for this ArtifactExtraFieldsData.
     * 
     * @param extra_field_id
     */
    public void setExtra_field_id(int extra_field_id) {
        this.extra_field_id = extra_field_id;
    }


    /**
     * Gets the field_data value for this ArtifactExtraFieldsData.
     * 
     * @return field_data
     */
    public java.lang.String getField_data() {
        return field_data;
    }


    /**
     * Sets the field_data value for this ArtifactExtraFieldsData.
     * 
     * @param field_data
     */
    public void setField_data(java.lang.String field_data) {
        this.field_data = field_data;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArtifactExtraFieldsData)) return false;
        ArtifactExtraFieldsData other = (ArtifactExtraFieldsData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.extra_field_id == other.getExtra_field_id() &&
            ((this.field_data==null && other.getField_data()==null) || 
             (this.field_data!=null &&
              this.field_data.equals(other.getField_data())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getExtra_field_id();
        if (getField_data() != null) {
            _hashCode += getField_data().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArtifactExtraFieldsData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraFieldsData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field_data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "field_data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
