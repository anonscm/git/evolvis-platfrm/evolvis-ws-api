/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ProjectTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ProjectTask  implements java.io.Serializable {
    private int project_task_id;

    private int group_project_id;

    private java.lang.String summary;

    private java.lang.String details;

    private int percent_complete;

    private int priority;

    private int hours;

    private int start_date;

    private int end_date;

    private int status_id;

    private int category_id;

    private org.evolvis.TaskDependency[] dependent_on;

    private org.evolvis.TaskAssignee[] assigned_to;

    private int duration;

    private int parent_id;

    private int sort_id;

    public ProjectTask() {
    }

    public ProjectTask(
           int project_task_id,
           int group_project_id,
           java.lang.String summary,
           java.lang.String details,
           int percent_complete,
           int priority,
           int hours,
           int start_date,
           int end_date,
           int status_id,
           int category_id,
           org.evolvis.TaskDependency[] dependent_on,
           org.evolvis.TaskAssignee[] assigned_to,
           int duration,
           int parent_id,
           int sort_id) {
           this.project_task_id = project_task_id;
           this.group_project_id = group_project_id;
           this.summary = summary;
           this.details = details;
           this.percent_complete = percent_complete;
           this.priority = priority;
           this.hours = hours;
           this.start_date = start_date;
           this.end_date = end_date;
           this.status_id = status_id;
           this.category_id = category_id;
           this.dependent_on = dependent_on;
           this.assigned_to = assigned_to;
           this.duration = duration;
           this.parent_id = parent_id;
           this.sort_id = sort_id;
    }


    /**
     * Gets the project_task_id value for this ProjectTask.
     * 
     * @return project_task_id
     */
    public int getProject_task_id() {
        return project_task_id;
    }


    /**
     * Sets the project_task_id value for this ProjectTask.
     * 
     * @param project_task_id
     */
    public void setProject_task_id(int project_task_id) {
        this.project_task_id = project_task_id;
    }


    /**
     * Gets the group_project_id value for this ProjectTask.
     * 
     * @return group_project_id
     */
    public int getGroup_project_id() {
        return group_project_id;
    }


    /**
     * Sets the group_project_id value for this ProjectTask.
     * 
     * @param group_project_id
     */
    public void setGroup_project_id(int group_project_id) {
        this.group_project_id = group_project_id;
    }


    /**
     * Gets the summary value for this ProjectTask.
     * 
     * @return summary
     */
    public java.lang.String getSummary() {
        return summary;
    }


    /**
     * Sets the summary value for this ProjectTask.
     * 
     * @param summary
     */
    public void setSummary(java.lang.String summary) {
        this.summary = summary;
    }


    /**
     * Gets the details value for this ProjectTask.
     * 
     * @return details
     */
    public java.lang.String getDetails() {
        return details;
    }


    /**
     * Sets the details value for this ProjectTask.
     * 
     * @param details
     */
    public void setDetails(java.lang.String details) {
        this.details = details;
    }


    /**
     * Gets the percent_complete value for this ProjectTask.
     * 
     * @return percent_complete
     */
    public int getPercent_complete() {
        return percent_complete;
    }


    /**
     * Sets the percent_complete value for this ProjectTask.
     * 
     * @param percent_complete
     */
    public void setPercent_complete(int percent_complete) {
        this.percent_complete = percent_complete;
    }


    /**
     * Gets the priority value for this ProjectTask.
     * 
     * @return priority
     */
    public int getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this ProjectTask.
     * 
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }


    /**
     * Gets the hours value for this ProjectTask.
     * 
     * @return hours
     */
    public int getHours() {
        return hours;
    }


    /**
     * Sets the hours value for this ProjectTask.
     * 
     * @param hours
     */
    public void setHours(int hours) {
        this.hours = hours;
    }


    /**
     * Gets the start_date value for this ProjectTask.
     * 
     * @return start_date
     */
    public int getStart_date() {
        return start_date;
    }


    /**
     * Sets the start_date value for this ProjectTask.
     * 
     * @param start_date
     */
    public void setStart_date(int start_date) {
        this.start_date = start_date;
    }


    /**
     * Gets the end_date value for this ProjectTask.
     * 
     * @return end_date
     */
    public int getEnd_date() {
        return end_date;
    }


    /**
     * Sets the end_date value for this ProjectTask.
     * 
     * @param end_date
     */
    public void setEnd_date(int end_date) {
        this.end_date = end_date;
    }


    /**
     * Gets the status_id value for this ProjectTask.
     * 
     * @return status_id
     */
    public int getStatus_id() {
        return status_id;
    }


    /**
     * Sets the status_id value for this ProjectTask.
     * 
     * @param status_id
     */
    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }


    /**
     * Gets the category_id value for this ProjectTask.
     * 
     * @return category_id
     */
    public int getCategory_id() {
        return category_id;
    }


    /**
     * Sets the category_id value for this ProjectTask.
     * 
     * @param category_id
     */
    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }


    /**
     * Gets the dependent_on value for this ProjectTask.
     * 
     * @return dependent_on
     */
    public org.evolvis.TaskDependency[] getDependent_on() {
        return dependent_on;
    }


    /**
     * Sets the dependent_on value for this ProjectTask.
     * 
     * @param dependent_on
     */
    public void setDependent_on(org.evolvis.TaskDependency[] dependent_on) {
        this.dependent_on = dependent_on;
    }


    /**
     * Gets the assigned_to value for this ProjectTask.
     * 
     * @return assigned_to
     */
    public org.evolvis.TaskAssignee[] getAssigned_to() {
        return assigned_to;
    }


    /**
     * Sets the assigned_to value for this ProjectTask.
     * 
     * @param assigned_to
     */
    public void setAssigned_to(org.evolvis.TaskAssignee[] assigned_to) {
        this.assigned_to = assigned_to;
    }


    /**
     * Gets the duration value for this ProjectTask.
     * 
     * @return duration
     */
    public int getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this ProjectTask.
     * 
     * @param duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }


    /**
     * Gets the parent_id value for this ProjectTask.
     * 
     * @return parent_id
     */
    public int getParent_id() {
        return parent_id;
    }


    /**
     * Sets the parent_id value for this ProjectTask.
     * 
     * @param parent_id
     */
    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }


    /**
     * Gets the sort_id value for this ProjectTask.
     * 
     * @return sort_id
     */
    public int getSort_id() {
        return sort_id;
    }


    /**
     * Sets the sort_id value for this ProjectTask.
     * 
     * @param sort_id
     */
    public void setSort_id(int sort_id) {
        this.sort_id = sort_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProjectTask)) return false;
        ProjectTask other = (ProjectTask) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.project_task_id == other.getProject_task_id() &&
            this.group_project_id == other.getGroup_project_id() &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary()))) &&
            ((this.details==null && other.getDetails()==null) || 
             (this.details!=null &&
              this.details.equals(other.getDetails()))) &&
            this.percent_complete == other.getPercent_complete() &&
            this.priority == other.getPriority() &&
            this.hours == other.getHours() &&
            this.start_date == other.getStart_date() &&
            this.end_date == other.getEnd_date() &&
            this.status_id == other.getStatus_id() &&
            this.category_id == other.getCategory_id() &&
            ((this.dependent_on==null && other.getDependent_on()==null) || 
             (this.dependent_on!=null &&
              java.util.Arrays.equals(this.dependent_on, other.getDependent_on()))) &&
            ((this.assigned_to==null && other.getAssigned_to()==null) || 
             (this.assigned_to!=null &&
              java.util.Arrays.equals(this.assigned_to, other.getAssigned_to()))) &&
            this.duration == other.getDuration() &&
            this.parent_id == other.getParent_id() &&
            this.sort_id == other.getSort_id();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getProject_task_id();
        _hashCode += getGroup_project_id();
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        if (getDetails() != null) {
            _hashCode += getDetails().hashCode();
        }
        _hashCode += getPercent_complete();
        _hashCode += getPriority();
        _hashCode += getHours();
        _hashCode += getStart_date();
        _hashCode += getEnd_date();
        _hashCode += getStatus_id();
        _hashCode += getCategory_id();
        if (getDependent_on() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDependent_on());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDependent_on(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAssigned_to() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssigned_to());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssigned_to(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getDuration();
        _hashCode += getParent_id();
        _hashCode += getSort_id();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProjectTask.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ProjectTask"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("project_task_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "project_task_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_project_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_project_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("summary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "summary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("percent_complete");
        elemField.setXmlName(new javax.xml.namespace.QName("", "percent_complete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hours");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("start_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "start_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("end_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "end_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dependent_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dependent_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "TaskDependency"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_to");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assigned_to"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "TaskAssignee"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("", "duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parent_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parent_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sort_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sort_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
