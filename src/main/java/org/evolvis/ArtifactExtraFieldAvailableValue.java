/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ArtifactExtraFieldAvailableValue.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ArtifactExtraFieldAvailableValue  implements java.io.Serializable {
    private int element_id;

    private java.lang.String element_name;

    private int status_id;

    public ArtifactExtraFieldAvailableValue() {
    }

    public ArtifactExtraFieldAvailableValue(
           int element_id,
           java.lang.String element_name,
           int status_id) {
           this.element_id = element_id;
           this.element_name = element_name;
           this.status_id = status_id;
    }


    /**
     * Gets the element_id value for this ArtifactExtraFieldAvailableValue.
     * 
     * @return element_id
     */
    public int getElement_id() {
        return element_id;
    }


    /**
     * Sets the element_id value for this ArtifactExtraFieldAvailableValue.
     * 
     * @param element_id
     */
    public void setElement_id(int element_id) {
        this.element_id = element_id;
    }


    /**
     * Gets the element_name value for this ArtifactExtraFieldAvailableValue.
     * 
     * @return element_name
     */
    public java.lang.String getElement_name() {
        return element_name;
    }


    /**
     * Sets the element_name value for this ArtifactExtraFieldAvailableValue.
     * 
     * @param element_name
     */
    public void setElement_name(java.lang.String element_name) {
        this.element_name = element_name;
    }


    /**
     * Gets the status_id value for this ArtifactExtraFieldAvailableValue.
     * 
     * @return status_id
     */
    public int getStatus_id() {
        return status_id;
    }


    /**
     * Sets the status_id value for this ArtifactExtraFieldAvailableValue.
     * 
     * @param status_id
     */
    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArtifactExtraFieldAvailableValue)) return false;
        ArtifactExtraFieldAvailableValue other = (ArtifactExtraFieldAvailableValue) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.element_id == other.getElement_id() &&
            ((this.element_name==null && other.getElement_name()==null) || 
             (this.element_name!=null &&
              this.element_name.equals(other.getElement_name()))) &&
            this.status_id == other.getStatus_id();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getElement_id();
        if (getElement_name() != null) {
            _hashCode += getElement_name().hashCode();
        }
        _hashCode += getStatus_id();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArtifactExtraFieldAvailableValue.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraFieldAvailableValue"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("element_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "element_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("element_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "element_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
