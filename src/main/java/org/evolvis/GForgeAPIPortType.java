/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * GForgeAPIPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public interface GForgeAPIPortType extends java.rmi.Remote {
    public java.lang.String login(java.lang.String userid, java.lang.String passwd) throws java.rmi.RemoteException;
    public java.lang.String logout(java.lang.String session_ser) throws java.rmi.RemoteException;
    public org.evolvis.Group[] getGroups(java.lang.String session_ser, int[] group_ids) throws java.rmi.RemoteException;
    public org.evolvis.Group[] getGroupsByName(java.lang.String session_ser, java.lang.String[] group_names) throws java.rmi.RemoteException;
    public java.lang.String[] getPublicProjectNames(java.lang.String session_ser) throws java.rmi.RemoteException;
    public org.evolvis.User[] getUsers(java.lang.String session_ser, int[] user_ids) throws java.rmi.RemoteException;
    public org.evolvis.User[] getUsersByName(java.lang.String session_ser, java.lang.String[] user_ids) throws java.rmi.RemoteException;
    public org.evolvis.Group[] userGetGroups(java.lang.String session_ser, int user_id) throws java.rmi.RemoteException;
    public org.evolvis.ArtifactType[] getArtifactTypes(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException;
    public org.evolvis.Artifact[] getArtifacts(java.lang.String session_ser, int group_id, int group_artifact_id, int assigned_to, int status) throws java.rmi.RemoteException;
    public int addArtifact(java.lang.String session_ser, int group_id, int group_artifact_id, int status_id, int priority, int assigned_to, java.lang.String summary, java.lang.String details, org.evolvis.ArtifactExtraFieldsData[] extra_fields) throws java.rmi.RemoteException;
    public int updateArtifact(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int status_id, int priority, int assigned_to, java.lang.String summary, java.lang.String details, int new_artifact_type_id, org.evolvis.ArtifactExtraFieldsData[] extra_fields_data) throws java.rmi.RemoteException;
    public org.evolvis.ArtifactFile[] getArtifactFiles(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public java.lang.String getArtifactFileData(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int file_id) throws java.rmi.RemoteException;
    public int addArtifactFile(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, java.lang.String base64_contents, java.lang.String description, java.lang.String filename, java.lang.String filetype) throws java.rmi.RemoteException;
    public boolean artifactFileDelete(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, int file_id) throws java.rmi.RemoteException;
    public org.evolvis.ArtifactMessage[] getArtifactMessages(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public int addArtifactMessage(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id, java.lang.String body) throws java.rmi.RemoteException;
    public org.evolvis.User[] getArtifactTechnicians(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException;
    public boolean artifactSetMonitor(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public boolean artifactIsMonitoring(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public boolean artifactDelete(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public boolean artifactTypeIsMonitoring(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException;
    public org.evolvis.ArtifactChangeLog[] artifactGetChangeLog(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_id) throws java.rmi.RemoteException;
    public org.evolvis.ArtifactQuery[] artifactGetViews(java.lang.String session_ser, int group_id, int group_artifact_id) throws java.rmi.RemoteException;
    public boolean artifactDeleteView(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_query_id) throws java.rmi.RemoteException;
    public boolean artifactSetView(java.lang.String session_ser, int group_id, int group_artifact_id, int artifact_query_id) throws java.rmi.RemoteException;
    public int artifactCreateView(java.lang.String session_ser, int group_id, int group_artifact_id, java.lang.String name, int status, int[] assignee, java.lang.String moddaterange, java.lang.String sort_col, java.lang.String sort_ord, org.evolvis.ArtifactExtraFieldsData[] extra_fields, java.lang.String opendaterange, java.lang.String closedaterange) throws java.rmi.RemoteException;
    public int artifactUpdateView(java.lang.String session_ser, int group_id, int group_artifact_id, int query_id, java.lang.String name, int status, int[] assignee, java.lang.String moddaterange, java.lang.String sort_col, java.lang.String sort_ord, org.evolvis.ArtifactExtraFieldsData[] extra_fields, java.lang.String opendaterange, java.lang.String closedaterange) throws java.rmi.RemoteException;
    public org.evolvis.ProjectGroup[] getProjectGroups(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException;
    public org.evolvis.ProjectTask[] getProjectTasks(java.lang.String session_ser, int group_id, int group_project_id, int assigned_to, int status, int category, int group) throws java.rmi.RemoteException;
    public int addProjectTask(java.lang.String session_ser, int group_id, int group_project_id, java.lang.String summary, java.lang.String details, int priority, int hours, int start_date, int end_date, int category_id, int percent_complete, int[] assigned_to, int[] dependent_on, int duration, int parent_id) throws java.rmi.RemoteException;
    public int updateProjectTask(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id, java.lang.String summary, java.lang.String details, int priority, int hours, int start_date, int end_date, int status_id, int category_id, int percent_complete, int[] assigned_to, int[] dependent_on, int new_group_project_id, int duration, int parent_id) throws java.rmi.RemoteException;
    public org.evolvis.ProjectCategory[] getProjectTaskCategories(java.lang.String session_ser, int group_id, int group_project_id) throws java.rmi.RemoteException;
    public org.evolvis.ProjectMessage[] getProjectMessages(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id) throws java.rmi.RemoteException;
    public int addProjectMessage(java.lang.String session_ser, int group_id, int group_project_id, int project_task_id, java.lang.String body) throws java.rmi.RemoteException;
    public org.evolvis.User[] getProjectTechnicians(java.lang.String session_ser, int group_id, int group_project_id) throws java.rmi.RemoteException;
    public org.evolvis.FRSPackage[] getPackages(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException;
    public int addPackage(java.lang.String session_ser, int group_id, java.lang.String package_name, int is_public) throws java.rmi.RemoteException;
    public org.evolvis.FRSRelease[] getReleases(java.lang.String session_ser, int group_id, int package_id) throws java.rmi.RemoteException;
    public int addRelease(java.lang.String session_ser, int group_id, int package_id, java.lang.String name, java.lang.String notes, java.lang.String changes, int release_date) throws java.rmi.RemoteException;
    public org.evolvis.FRSFile[] getFiles(java.lang.String session_ser, int group_id, int package_id, int release_id) throws java.rmi.RemoteException;
    public java.lang.String getFile(java.lang.String session_ser, int group_id, int package_id, int release_id, int file_id) throws java.rmi.RemoteException;
    public java.lang.String addFile(java.lang.String session_ser, int group_id, int package_id, int release_id, java.lang.String name, java.lang.String base64_contents, int type_id, int processor_id, int release_time) throws java.rmi.RemoteException;
    public org.evolvis.DocumentState[] getDocumentStates(java.lang.String session_ser) throws java.rmi.RemoteException;
    public org.evolvis.DocumentLanguage[] getDocumentLanguages(java.lang.String session_ser) throws java.rmi.RemoteException;
    public int addDocument(java.lang.String session_ser, int group_id, int doc_group, java.lang.String title, java.lang.String description, int language_id, java.lang.String base64_contents, java.lang.String filename, java.lang.String file_url) throws java.rmi.RemoteException;
    public boolean updateDocument(java.lang.String session_ser, int group_id, int doc_group, int doc_id, java.lang.String title, java.lang.String description, int language_id, java.lang.String base64_contents, java.lang.String filename, java.lang.String file_url, int state_id) throws java.rmi.RemoteException;
    public int addDocumentGroup(java.lang.String session_ser, int group_id, java.lang.String groupname, int parent_doc_group) throws java.rmi.RemoteException;
    public boolean updateDocumentGroup(java.lang.String session_ser, int group_id, int doc_group, java.lang.String new_groupname, int new_parent_doc_group) throws java.rmi.RemoteException;
    public org.evolvis.Document[] getDocuments(java.lang.String session_ser, int group_id, int doc_group) throws java.rmi.RemoteException;
    public org.evolvis.DocumentGroup[] getDocumentGroups(java.lang.String session_ser, int group_id) throws java.rmi.RemoteException;
    public org.evolvis.DocumentGroup getDocumentGroup(java.lang.String session_ser, int group_id, int doc_group) throws java.rmi.RemoteException;
    public org.evolvis.DocumentFile[] getDocumentFiles(java.lang.String session_ser, int group_id, int doc_id) throws java.rmi.RemoteException;
    public boolean documentDelete(java.lang.String session_ser, int group_id, int doc_id) throws java.rmi.RemoteException;
}
