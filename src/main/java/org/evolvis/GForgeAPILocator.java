/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * GForgeAPILocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class GForgeAPILocator extends org.apache.axis.client.Service implements org.evolvis.GForgeAPI {

    public GForgeAPILocator() {
    }


    public GForgeAPILocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GForgeAPILocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GForgeAPIPort
    private java.lang.String GForgeAPIPort_address = "http://evolvis.org/soap/index.php";

    public java.lang.String getGForgeAPIPortAddress() {
        return GForgeAPIPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GForgeAPIPortWSDDServiceName = "GForgeAPIPort";

    public java.lang.String getGForgeAPIPortWSDDServiceName() {
        return GForgeAPIPortWSDDServiceName;
    }

    public void setGForgeAPIPortWSDDServiceName(java.lang.String name) {
        GForgeAPIPortWSDDServiceName = name;
    }

    public org.evolvis.GForgeAPIPortType getGForgeAPIPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GForgeAPIPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGForgeAPIPort(endpoint);
    }

    public org.evolvis.GForgeAPIPortType getGForgeAPIPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.evolvis.GForgeAPIBindingStub _stub = new org.evolvis.GForgeAPIBindingStub(portAddress, this);
            _stub.setPortName(getGForgeAPIPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGForgeAPIPortEndpointAddress(java.lang.String address) {
        GForgeAPIPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.evolvis.GForgeAPIPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                org.evolvis.GForgeAPIBindingStub _stub = new org.evolvis.GForgeAPIBindingStub(new java.net.URL(GForgeAPIPort_address), this);
                _stub.setPortName(getGForgeAPIPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GForgeAPIPort".equals(inputPortName)) {
            return getGForgeAPIPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://evolvis.org", "GForgeAPI");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://evolvis.org", "GForgeAPIPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GForgeAPIPort".equals(portName)) {
            setGForgeAPIPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
