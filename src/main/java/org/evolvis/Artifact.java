/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * Artifact.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class Artifact  implements java.io.Serializable {
    private int artifact_id;

    private int group_artifact_id;

    private int status_id;

    private int priority;

    private int submitted_by;

    private int assigned_to;

    private int open_date;

    private int close_date;

    private java.lang.String summary;

    private java.lang.String details;

    private org.evolvis.ArtifactExtraFieldsData[] extra_fields;

    public Artifact() {
    }

    public Artifact(
           int artifact_id,
           int group_artifact_id,
           int status_id,
           int priority,
           int submitted_by,
           int assigned_to,
           int open_date,
           int close_date,
           java.lang.String summary,
           java.lang.String details,
           org.evolvis.ArtifactExtraFieldsData[] extra_fields) {
           this.artifact_id = artifact_id;
           this.group_artifact_id = group_artifact_id;
           this.status_id = status_id;
           this.priority = priority;
           this.submitted_by = submitted_by;
           this.assigned_to = assigned_to;
           this.open_date = open_date;
           this.close_date = close_date;
           this.summary = summary;
           this.details = details;
           this.extra_fields = extra_fields;
    }


    /**
     * Gets the artifact_id value for this Artifact.
     * 
     * @return artifact_id
     */
    public int getArtifact_id() {
        return artifact_id;
    }


    /**
     * Sets the artifact_id value for this Artifact.
     * 
     * @param artifact_id
     */
    public void setArtifact_id(int artifact_id) {
        this.artifact_id = artifact_id;
    }


    /**
     * Gets the group_artifact_id value for this Artifact.
     * 
     * @return group_artifact_id
     */
    public int getGroup_artifact_id() {
        return group_artifact_id;
    }


    /**
     * Sets the group_artifact_id value for this Artifact.
     * 
     * @param group_artifact_id
     */
    public void setGroup_artifact_id(int group_artifact_id) {
        this.group_artifact_id = group_artifact_id;
    }


    /**
     * Gets the status_id value for this Artifact.
     * 
     * @return status_id
     */
    public int getStatus_id() {
        return status_id;
    }


    /**
     * Sets the status_id value for this Artifact.
     * 
     * @param status_id
     */
    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }


    /**
     * Gets the priority value for this Artifact.
     * 
     * @return priority
     */
    public int getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this Artifact.
     * 
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }


    /**
     * Gets the submitted_by value for this Artifact.
     * 
     * @return submitted_by
     */
    public int getSubmitted_by() {
        return submitted_by;
    }


    /**
     * Sets the submitted_by value for this Artifact.
     * 
     * @param submitted_by
     */
    public void setSubmitted_by(int submitted_by) {
        this.submitted_by = submitted_by;
    }


    /**
     * Gets the assigned_to value for this Artifact.
     * 
     * @return assigned_to
     */
    public int getAssigned_to() {
        return assigned_to;
    }


    /**
     * Sets the assigned_to value for this Artifact.
     * 
     * @param assigned_to
     */
    public void setAssigned_to(int assigned_to) {
        this.assigned_to = assigned_to;
    }


    /**
     * Gets the open_date value for this Artifact.
     * 
     * @return open_date
     */
    public int getOpen_date() {
        return open_date;
    }


    /**
     * Sets the open_date value for this Artifact.
     * 
     * @param open_date
     */
    public void setOpen_date(int open_date) {
        this.open_date = open_date;
    }


    /**
     * Gets the close_date value for this Artifact.
     * 
     * @return close_date
     */
    public int getClose_date() {
        return close_date;
    }


    /**
     * Sets the close_date value for this Artifact.
     * 
     * @param close_date
     */
    public void setClose_date(int close_date) {
        this.close_date = close_date;
    }


    /**
     * Gets the summary value for this Artifact.
     * 
     * @return summary
     */
    public java.lang.String getSummary() {
        return summary;
    }


    /**
     * Sets the summary value for this Artifact.
     * 
     * @param summary
     */
    public void setSummary(java.lang.String summary) {
        this.summary = summary;
    }


    /**
     * Gets the details value for this Artifact.
     * 
     * @return details
     */
    public java.lang.String getDetails() {
        return details;
    }


    /**
     * Sets the details value for this Artifact.
     * 
     * @param details
     */
    public void setDetails(java.lang.String details) {
        this.details = details;
    }


    /**
     * Gets the extra_fields value for this Artifact.
     * 
     * @return extra_fields
     */
    public org.evolvis.ArtifactExtraFieldsData[] getExtra_fields() {
        return extra_fields;
    }


    /**
     * Sets the extra_fields value for this Artifact.
     * 
     * @param extra_fields
     */
    public void setExtra_fields(org.evolvis.ArtifactExtraFieldsData[] extra_fields) {
        this.extra_fields = extra_fields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Artifact)) return false;
        Artifact other = (Artifact) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.artifact_id == other.getArtifact_id() &&
            this.group_artifact_id == other.getGroup_artifact_id() &&
            this.status_id == other.getStatus_id() &&
            this.priority == other.getPriority() &&
            this.submitted_by == other.getSubmitted_by() &&
            this.assigned_to == other.getAssigned_to() &&
            this.open_date == other.getOpen_date() &&
            this.close_date == other.getClose_date() &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary()))) &&
            ((this.details==null && other.getDetails()==null) || 
             (this.details!=null &&
              this.details.equals(other.getDetails()))) &&
            ((this.extra_fields==null && other.getExtra_fields()==null) || 
             (this.extra_fields!=null &&
              java.util.Arrays.equals(this.extra_fields, other.getExtra_fields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getArtifact_id();
        _hashCode += getGroup_artifact_id();
        _hashCode += getStatus_id();
        _hashCode += getPriority();
        _hashCode += getSubmitted_by();
        _hashCode += getAssigned_to();
        _hashCode += getOpen_date();
        _hashCode += getClose_date();
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        if (getDetails() != null) {
            _hashCode += getDetails().hashCode();
        }
        if (getExtra_fields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExtra_fields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtra_fields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Artifact.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "Artifact"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("artifact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "artifact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_artifact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_artifact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submitted_by");
        elemField.setXmlName(new javax.xml.namespace.QName("", "submitted_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_to");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assigned_to"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("open_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "open_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("close_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "close_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("summary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "summary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_fields");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_fields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraFieldsData"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
