/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * ArtifactExtraField.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class ArtifactExtraField  implements java.io.Serializable {
    private int extra_field_id;

    private java.lang.String field_name;

    private int field_type;

    private int attribute1;

    private int attribute2;

    private int is_required;

    private java.lang.String alias;

    private org.evolvis.ArtifactExtraFieldAvailableValue[] available_values;

    private int default_selected_id;

    public ArtifactExtraField() {
    }

    public ArtifactExtraField(
           int extra_field_id,
           java.lang.String field_name,
           int field_type,
           int attribute1,
           int attribute2,
           int is_required,
           java.lang.String alias,
           org.evolvis.ArtifactExtraFieldAvailableValue[] available_values,
           int default_selected_id) {
           this.extra_field_id = extra_field_id;
           this.field_name = field_name;
           this.field_type = field_type;
           this.attribute1 = attribute1;
           this.attribute2 = attribute2;
           this.is_required = is_required;
           this.alias = alias;
           this.available_values = available_values;
           this.default_selected_id = default_selected_id;
    }


    /**
     * Gets the extra_field_id value for this ArtifactExtraField.
     * 
     * @return extra_field_id
     */
    public int getExtra_field_id() {
        return extra_field_id;
    }


    /**
     * Sets the extra_field_id value for this ArtifactExtraField.
     * 
     * @param extra_field_id
     */
    public void setExtra_field_id(int extra_field_id) {
        this.extra_field_id = extra_field_id;
    }


    /**
     * Gets the field_name value for this ArtifactExtraField.
     * 
     * @return field_name
     */
    public java.lang.String getField_name() {
        return field_name;
    }


    /**
     * Sets the field_name value for this ArtifactExtraField.
     * 
     * @param field_name
     */
    public void setField_name(java.lang.String field_name) {
        this.field_name = field_name;
    }


    /**
     * Gets the field_type value for this ArtifactExtraField.
     * 
     * @return field_type
     */
    public int getField_type() {
        return field_type;
    }


    /**
     * Sets the field_type value for this ArtifactExtraField.
     * 
     * @param field_type
     */
    public void setField_type(int field_type) {
        this.field_type = field_type;
    }


    /**
     * Gets the attribute1 value for this ArtifactExtraField.
     * 
     * @return attribute1
     */
    public int getAttribute1() {
        return attribute1;
    }


    /**
     * Sets the attribute1 value for this ArtifactExtraField.
     * 
     * @param attribute1
     */
    public void setAttribute1(int attribute1) {
        this.attribute1 = attribute1;
    }


    /**
     * Gets the attribute2 value for this ArtifactExtraField.
     * 
     * @return attribute2
     */
    public int getAttribute2() {
        return attribute2;
    }


    /**
     * Sets the attribute2 value for this ArtifactExtraField.
     * 
     * @param attribute2
     */
    public void setAttribute2(int attribute2) {
        this.attribute2 = attribute2;
    }


    /**
     * Gets the is_required value for this ArtifactExtraField.
     * 
     * @return is_required
     */
    public int getIs_required() {
        return is_required;
    }


    /**
     * Sets the is_required value for this ArtifactExtraField.
     * 
     * @param is_required
     */
    public void setIs_required(int is_required) {
        this.is_required = is_required;
    }


    /**
     * Gets the alias value for this ArtifactExtraField.
     * 
     * @return alias
     */
    public java.lang.String getAlias() {
        return alias;
    }


    /**
     * Sets the alias value for this ArtifactExtraField.
     * 
     * @param alias
     */
    public void setAlias(java.lang.String alias) {
        this.alias = alias;
    }


    /**
     * Gets the available_values value for this ArtifactExtraField.
     * 
     * @return available_values
     */
    public org.evolvis.ArtifactExtraFieldAvailableValue[] getAvailable_values() {
        return available_values;
    }


    /**
     * Sets the available_values value for this ArtifactExtraField.
     * 
     * @param available_values
     */
    public void setAvailable_values(org.evolvis.ArtifactExtraFieldAvailableValue[] available_values) {
        this.available_values = available_values;
    }


    /**
     * Gets the default_selected_id value for this ArtifactExtraField.
     * 
     * @return default_selected_id
     */
    public int getDefault_selected_id() {
        return default_selected_id;
    }


    /**
     * Sets the default_selected_id value for this ArtifactExtraField.
     * 
     * @param default_selected_id
     */
    public void setDefault_selected_id(int default_selected_id) {
        this.default_selected_id = default_selected_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArtifactExtraField)) return false;
        ArtifactExtraField other = (ArtifactExtraField) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.extra_field_id == other.getExtra_field_id() &&
            ((this.field_name==null && other.getField_name()==null) || 
             (this.field_name!=null &&
              this.field_name.equals(other.getField_name()))) &&
            this.field_type == other.getField_type() &&
            this.attribute1 == other.getAttribute1() &&
            this.attribute2 == other.getAttribute2() &&
            this.is_required == other.getIs_required() &&
            ((this.alias==null && other.getAlias()==null) || 
             (this.alias!=null &&
              this.alias.equals(other.getAlias()))) &&
            ((this.available_values==null && other.getAvailable_values()==null) || 
             (this.available_values!=null &&
              java.util.Arrays.equals(this.available_values, other.getAvailable_values()))) &&
            this.default_selected_id == other.getDefault_selected_id();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getExtra_field_id();
        if (getField_name() != null) {
            _hashCode += getField_name().hashCode();
        }
        _hashCode += getField_type();
        _hashCode += getAttribute1();
        _hashCode += getAttribute2();
        _hashCode += getIs_required();
        if (getAlias() != null) {
            _hashCode += getAlias().hashCode();
        }
        if (getAvailable_values() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailable_values());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailable_values(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getDefault_selected_id();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArtifactExtraField.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraField"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "field_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "field_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attribute1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attribute2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_required");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_required"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alias");
        elemField.setXmlName(new javax.xml.namespace.QName("", "alias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available_values");
        elemField.setXmlName(new javax.xml.namespace.QName("", "available_values"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "ArtifactExtraFieldAvailableValue"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("default_selected_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "default_selected_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
