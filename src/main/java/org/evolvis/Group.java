/*
 * evolvis-ws-api,
 * The webservice API for evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-ws-api'
 * Signature of Elmar Geese, 20 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * Group.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.evolvis;

public class Group  implements java.io.Serializable {
    private int group_id;

    private java.lang.String group_name;

    private java.lang.String homepage;

    private int is_public;

    private java.lang.String status;

    private java.lang.String unix_group_name;

    private java.lang.String short_description;

    private java.lang.String scm_box;

    private int register_time;

    public Group() {
    }

    public Group(
           int group_id,
           java.lang.String group_name,
           java.lang.String homepage,
           int is_public,
           java.lang.String status,
           java.lang.String unix_group_name,
           java.lang.String short_description,
           java.lang.String scm_box,
           int register_time) {
           this.group_id = group_id;
           this.group_name = group_name;
           this.homepage = homepage;
           this.is_public = is_public;
           this.status = status;
           this.unix_group_name = unix_group_name;
           this.short_description = short_description;
           this.scm_box = scm_box;
           this.register_time = register_time;
    }


    /**
     * Gets the group_id value for this Group.
     * 
     * @return group_id
     */
    public int getGroup_id() {
        return group_id;
    }


    /**
     * Sets the group_id value for this Group.
     * 
     * @param group_id
     */
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }


    /**
     * Gets the group_name value for this Group.
     * 
     * @return group_name
     */
    public java.lang.String getGroup_name() {
        return group_name;
    }


    /**
     * Sets the group_name value for this Group.
     * 
     * @param group_name
     */
    public void setGroup_name(java.lang.String group_name) {
        this.group_name = group_name;
    }


    /**
     * Gets the homepage value for this Group.
     * 
     * @return homepage
     */
    public java.lang.String getHomepage() {
        return homepage;
    }


    /**
     * Sets the homepage value for this Group.
     * 
     * @param homepage
     */
    public void setHomepage(java.lang.String homepage) {
        this.homepage = homepage;
    }


    /**
     * Gets the is_public value for this Group.
     * 
     * @return is_public
     */
    public int getIs_public() {
        return is_public;
    }


    /**
     * Sets the is_public value for this Group.
     * 
     * @param is_public
     */
    public void setIs_public(int is_public) {
        this.is_public = is_public;
    }


    /**
     * Gets the status value for this Group.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Group.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the unix_group_name value for this Group.
     * 
     * @return unix_group_name
     */
    public java.lang.String getUnix_group_name() {
        return unix_group_name;
    }


    /**
     * Sets the unix_group_name value for this Group.
     * 
     * @param unix_group_name
     */
    public void setUnix_group_name(java.lang.String unix_group_name) {
        this.unix_group_name = unix_group_name;
    }


    /**
     * Gets the short_description value for this Group.
     * 
     * @return short_description
     */
    public java.lang.String getShort_description() {
        return short_description;
    }


    /**
     * Sets the short_description value for this Group.
     * 
     * @param short_description
     */
    public void setShort_description(java.lang.String short_description) {
        this.short_description = short_description;
    }


    /**
     * Gets the scm_box value for this Group.
     * 
     * @return scm_box
     */
    public java.lang.String getScm_box() {
        return scm_box;
    }


    /**
     * Sets the scm_box value for this Group.
     * 
     * @param scm_box
     */
    public void setScm_box(java.lang.String scm_box) {
        this.scm_box = scm_box;
    }


    /**
     * Gets the register_time value for this Group.
     * 
     * @return register_time
     */
    public int getRegister_time() {
        return register_time;
    }


    /**
     * Sets the register_time value for this Group.
     * 
     * @param register_time
     */
    public void setRegister_time(int register_time) {
        this.register_time = register_time;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Group)) return false;
        Group other = (Group) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.group_id == other.getGroup_id() &&
            ((this.group_name==null && other.getGroup_name()==null) || 
             (this.group_name!=null &&
              this.group_name.equals(other.getGroup_name()))) &&
            ((this.homepage==null && other.getHomepage()==null) || 
             (this.homepage!=null &&
              this.homepage.equals(other.getHomepage()))) &&
            this.is_public == other.getIs_public() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.unix_group_name==null && other.getUnix_group_name()==null) || 
             (this.unix_group_name!=null &&
              this.unix_group_name.equals(other.getUnix_group_name()))) &&
            ((this.short_description==null && other.getShort_description()==null) || 
             (this.short_description!=null &&
              this.short_description.equals(other.getShort_description()))) &&
            ((this.scm_box==null && other.getScm_box()==null) || 
             (this.scm_box!=null &&
              this.scm_box.equals(other.getScm_box()))) &&
            this.register_time == other.getRegister_time();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGroup_id();
        if (getGroup_name() != null) {
            _hashCode += getGroup_name().hashCode();
        }
        if (getHomepage() != null) {
            _hashCode += getHomepage().hashCode();
        }
        _hashCode += getIs_public();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getUnix_group_name() != null) {
            _hashCode += getUnix_group_name().hashCode();
        }
        if (getShort_description() != null) {
            _hashCode += getShort_description().hashCode();
        }
        if (getScm_box() != null) {
            _hashCode += getScm_box().hashCode();
        }
        _hashCode += getRegister_time();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Group.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://evolvis.org", "Group"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "group_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("homepage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "homepage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_public");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_public"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unix_group_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unix_group_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("short_description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "short_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scm_box");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scm_box"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("register_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "register_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
